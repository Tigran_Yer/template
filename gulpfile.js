// Gulp
var gulp = require('gulp');

// Templates
var ejs = require('gulp-ejs');

// Stylesheet
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');
var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR'],
    cascade: false
};

// Rename Files
var rename = require("gulp-rename");

// Notify On Error
var notify = require('gulp-notify');

// Concat files
var concat = require('gulp-concat');

// Compress JS
var uglify = require('gulp-uglify');

// Compress HTML
var htmlmin = require('gulp-htmlmin');

// Rewrite images path
var rewriteImagePath = require('gulp-rewrite-image-path');

// Delete file/directory
var del = require('del');

// browserSync
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


/// Dev Mode Tasks \\\

// Transform ".scss", minify, rename, add vendor prefixes
gulp.task('styles', function() {
    gulp.src('./app/scss/main.scss')
        .pipe(sass())
        .on('error', function (err) {
            return notify().write(err)
        })
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/css'))
        .pipe(reload({stream: true}))
});

// Concat, compress and rename JS files into one
gulp.task('js', function() {
    gulp.src(['./app/js/libs/jquery.min.js', './app/js/libs/popper.min.js', './app/js/libs/bootstrap.min.js',
        './app/js/libs/scrollme.min.js', './app/js/main.js'])
        .pipe(uglify())
        .pipe(concat('all.js'))
        .on('error', function (err) {
            return notify().write(err)
        })
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/js/'))
        .pipe(reload({stream: true}))
});

// Rewrite "views/partials" inner images paths
gulp.task('rewriteImagePath', function() {
    gulp.src("./app/views/partials/**/*.ejs")
    .pipe(rewriteImagePath({path:"../img"}))
});

// Transform ".ejs" files to static ".html" files
gulp.task('html', ['rewriteImagePath'], function() {
    gulp.src('./app/views/*.ejs')
        .pipe(ejs({}, {}, {
            ext: '.html'
        }))
        .pipe(htmlmin({collapseWhitespace: true}))
        .on('error', function (err) {
            return notify().write(err)
        })
        .pipe(gulp.dest('./app/html/'))
        .pipe(reload({stream: true}))
});

// browserSync init
gulp.task('browserSync', function() {
    browserSync.init(null, {
        server: {baseDir: "./app"}
    });
});

// Watching files
gulp.task('watch', ['browserSync'], function() {
    gulp.watch(["./app/js/main.js"], ['js'], reload);
    gulp.watch(["./app/scss/**/*.scss"], ['styles'], reload);
    gulp.watch(["./app/views/**/*.ejs"], ['html'], reload);
});

gulp.task('default', ['watch', 'html', 'styles', 'js']);


/// Build Mode Tasks \\\

// Always delete "dist" directory before build
gulp.task('clean', function() {
    return del.sync('./dist');
});

// Build Project
gulp.task('build', ['clean'], function() {
    var buildHtml = gulp.src('./app/html/*')
        .pipe(gulp.dest('./dist/html'));
    var buildCss = gulp.src('./app/css/**/*')
        .pipe(gulp.dest('./dist/css'));
    var buildJs = gulp.src('./app/js/all.min.js')
        .pipe(gulp.dest('./dist/js'));
    var buildImages = gulp.src('./app/img/*')
        .pipe(gulp.dest('./dist/img'));
    var buildFonts = gulp.src('./app/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts'));
});

