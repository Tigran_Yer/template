$(window).on('load resize', function() {
     var mobileMenuContent = '<div class="nav-logo-icon d-flex justify-content-between align-items-center" id="navbarBrand">\n' +
         '            <a class="navbar-brand d-block" href="index.html">\n' +
         '                <!--future logo-->\n' +
         '                <!--<img src="" class="w-100" alt="Logo">-->\n' +
         '                <span class="text-white noto-bold fz-18">Logo</span>\n' +
         '            </a>\n' +
         '            <button class="navbar-toggler border-none" type="button" id="hamburgerIcon" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">\n' +
         '            <span class="navbar-toggler-icon">\n' +
         '                <svg viewBox="0 0 100 100" width="50" class="hamburger">\n' +
         '                    <path class="line top" d="m 70,33 h -40 c 0,0 -6,1.368796 -6,8.5 0,7.131204 6,8.5013 6,8.5013 l 20,-0.0013" />\n' +
         '                    <path class="line middle" d="m 70,50 h -40" />\n' +
         '                    <path class="line bottom" d="m 69.575405,67.073826 h -40 c -5.592752,0 -6.873604,-9.348582 1.371031,-9.348582 8.244634,0 19.053564,21.797129 19.053564,12.274756 l 0,-40" />\n' +
         '                </svg>\n' +
         '            </span>\n' +
         '            </button>\n' +
         '        </div>\n' +
         '        <div class="collapse navbar-collapse" id="navbarNav">\n' +
         '            <ul class="navbar-nav justify-content-md-end">\n' +
         '                <li class="nav-item d-flex align-items-center">\n' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20 relative z-1" data-href="introWork">work</a>\n' +
         '                </li>\n' +
         '                <li class="nav-item d-flex align-items-center">\n' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20 relative z-1" data-href="introStuff">stuff</a>\n' +
         '                </li>\n' +
         '                <li class="nav-item d-flex align-items-center">\n' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20 relative z-1" data-href="features">features</a>\n' +
         '                </li>\n' +
         '                <li class="nav-item d-flex align-items-center">\n' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20 relative z-1" data-href="tryUs">try us</a>\n' +
         '                </li>\n' +
         '                <li class="nav-item d-flex align-items-center">\n' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20 relative z-1" data-href="history">history</a>\n' +
         '                </li>\n' +
         '                <li class="nav-item d-flex align-items-center">\n' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20 relative z-1" data-href="contact">contact</a>\n' +
         '                </li>\n' +
         '            </ul>\n' +
         '        </div>';
     var desktopMenuContent = '<div class="main-wrapper d-flex">' +
         '<div class="nav-logo-icon d-flex align-items-center" id="navbarBrand">' +
         '            <a class="navbar-brand d-block" href="index.html">' +
         '                <span class="text-white noto-bold fz-18">Logo</span>' +
         '            </a>' +
         '        </div>' +
         '        <div class="collapse navbar-collapse" id="navbarNav">' +
         '            <ul class="navbar-nav justify-content-md-end" id="navScrollSpy">' +
         '                <li class="nav-item d-flex align-items-center nowrap">' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20" href="#introWork" data-href="introWork">work</a>' +
         '                </li>' +
         '                <li class="nav-item d-flex align-items-center nowrap">' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20" href="#introStuff" data-href="introStuff">stuff</a>' +
         '                </li>' +
         '                <li class="nav-item d-flex align-items-center nowrap">' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20" href="#features" data-href="features">features</a>' +
         '                </li>' +
         '                <li class="nav-item d-flex align-items-center nowrap">' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20" href="#tryUs" data-href="tryUs">try us</a>' +
         '                </li>' +
         '                <li class="nav-item d-flex align-items-center nowrap">' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20" href="#history" data-href="history">history</a>' +
         '                </li>' +
         '                <li class="nav-item d-flex align-items-center nowrap">' +
         '                    <a class="nav-link pd-0 tt-up noto-bold fz-20" href="#contact" data-href="contact">contact</a>' +
         '                </li>' +
         '            </ul>' +
         '        </div>' +
         '</div>';
    var nav = document.getElementById('navWrapper');
    var desktopSize = window.matchMedia("(min-width: 768px)").matches;
     if (desktopSize) {
         nav.innerHTML = desktopMenuContent;
     } else {
         nav.innerHTML = mobileMenuContent;
     }
});

$(window).on('load', function() {
    var scrollToElem = function() {
        var navlink = $('.nav-link');
        var navlinks = navlink.toArray();
        navlinks.forEach(function (item) {
            var nav = $('#navbarNav');
            var hamburger = $('#hamburgerIcon');
            var href = $(item).attr("data-href");
            var target = document.getElementById(href);
            var targetTop = target.offsetTop;
            var headerNav = $('#navWrapper');
            var headerNavHeight = headerNav.innerHeight() + 1;
            var absScrollPos = targetTop - headerNavHeight;
            var desktopSize = window.matchMedia("(min-width: 768px)").matches;
            $(item).on('click', function(e) {
                hamburger.attr("aria-expanded","false");
                nav.removeClass('collapsing');
                nav.removeClass('show');
                navlink.removeClass('active');
                $(item).addClass('active');
                if (desktopSize) {
                    e.preventDefault();
                    $('html, body').animate({
                        scrollTop: absScrollPos
                    }, 900);
                } else {
                    $('main').animate({
                        scrollTop: absScrollPos + 70
                    }, 900);
                }
            });
        })
    };
    $('[data-spy="scroll"]').each(function () {
        var $spy = $(this).scrollspy('refresh')
    });
    scrollToElem();
});